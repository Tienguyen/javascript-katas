import {charCount} from "./character-count.ts";

it('test "edabit" with char "a", should return 1', function (){
    expect(
        charCount("a", "edabit")).toBe(1)
})

it('test "Chamber of secrets" with char "c", should return 1', function (){
    expect(
        charCount("c", "Chamber of secrets")).toBe(1)
})

it('test "Boxes are fun" with char "B", should return 0', function (){
    expect(
        charCount("B", "boxes are fun")).toBe(0)
})

it('test "big fat bubble" with char "b", should return 4', function (){
    expect(
        charCount("b", "big fat bubble")).toBe(4)
})

it('test "e" with char "javascript is good", should return 0', function (){
    expect(
        charCount("e", "javascript is good")).toBe(0)
})

it('test "!" with char "!easy!", should return 2', function (){
    expect(
        charCount("!", "!easy!")).toBe(2)
})