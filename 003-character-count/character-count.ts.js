export function charCount(char, phrase){
    let count = 0;
    for(let i = 0; i < phrase.length; i++) {
        if(phrase.charAt(i) == char){
            count += 1;
        }
    }
    return count;
}