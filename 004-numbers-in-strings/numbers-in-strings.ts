export function numInStr(strings:string[]): string[]{
    let strs:string[] = [];
    for (let string of strings) {
        for (let s of string) {
            // @ts-ignore
            if (!isNaN(s) && s != " ") {
                strs.push(string)
                break
            }
        }
    }
    return strs;
}