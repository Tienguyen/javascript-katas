import { numInStr } from "./numbers-in-strings";

it('[la, a, 2b, b], should return array with two strings', function (){
    expect(
        numInStr(["1a", "a", "2b", "b"])).toStrictEqual(["1a", "2b"])
})

it('[abc, abc10], should return one string', function (){
    expect(
        numInStr(["abc" ,"abc10"])).toStrictEqual(["abc10"])
})

it('[abc, ab10c, a10bc, bcd], should return array with two strings', function (){
    expect(
        numInStr(["abc", "ab10c", "a10bc", "bcd"])).toStrictEqual(["ab10c", "a10bc"])
})

it('[this is a test, test1], should return array with one strings', function (){
    expect(
        numInStr(["this is a test", "test1"])).toStrictEqual(["test1"])
})

it('[who needs numbers, not me], should return array with no strings', function (){
    expect(
        numInStr(["who needs numbers", "not me"])).toStrictEqual([])
})

it('[!!, ##, @], should return array with no strings', function (){
    expect(
        numInStr(["!!", "##", "@"])).toStrictEqual([])
})