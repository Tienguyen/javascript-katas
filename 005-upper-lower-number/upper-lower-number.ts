export function reorder(input : string){
    let upper:string[] = []
    let lower:string[] = [];
    let numbers:string[] = [];

    for(let s of input){
        // @ts-ignore
        if(!isNaN(s)){
            numbers.push(s)
        }else if(s == s.toUpperCase()){
            upper.push(s)
        }else{
            lower.push(s)
        }
    }

    let returnArray = upper.concat(lower, numbers).join("");
    return returnArray;
}

