import { reorder } from "./upper-lower-number";

it('should reorder "hA2p4Py" to "APhpy24"', function (){
    expect(
        reorder("hA2p4Py")).toBe("APhpy24")
})

it('should reorder "m11oveMENT" to "MENTmove11"', function (){
    expect(
        reorder("m11oveMENT")).toBe("MENTmove11")
})

it('should reorder "s9hOrt4CAKE" to "OCAKEshrt94"', function (){
    expect(
        reorder("s9hOrt4CAKE")).toBe("OCAKEshrt94")
})