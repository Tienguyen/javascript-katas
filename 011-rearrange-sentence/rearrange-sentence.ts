export function rearrange(input:string){
    let word_array = input.split(" ")
    const numbers = Array.from(Array(9).keys())
    let dict: Record<string, string> = {}
    let returnString = ""
    for(let word of word_array){
        for(let character of word){
            if (numbers.includes(parseInt(character))){
                dict[character] = word.replace(character, "")
            }
        }
    }

    for(let number in numbers){
        if(typeof dict[number.toString()] !== "undefined"){
            returnString += dict[number.toString()] + " "
        }
    }
    returnString = returnString.slice(0, -1)
    return returnString
}