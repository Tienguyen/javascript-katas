import { rearrange } from "./rearrange-sentence";

it("is2 Thi1s T4est 3a", function (){
    expect(
        rearrange("is2 Thi1s T4est 3a")).toBe("This is a Test")
})

it("4of Fo1r pe6ople g3ood th5e the2", function (){
    expect(
        rearrange("4of Fo1r pe6ople g3ood th5e the2")).toBe("For the good of the people")
})

it("5weird i2s JavaScri1pt dam4n so3", function (){
    expect(
        rearrange("5weird i2s JavaScri1pt dam4n so3")).toBe("JavaScript is so damn weird"
)
})

it('input empty string, should return empty string', function (){
    expect(
        rearrange("")).toBe("")
})
