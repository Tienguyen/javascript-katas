import { replaceVowel} from "./replace-vowel";

it('karAchi', function (){
    expect(
        replaceVowel("karAchi")).toBe("k1r1ch3")
})

it('chEmBur', function (){
    expect(
        replaceVowel("chEmBur")).toBe("ch2mb5r")
})

it('khandbari', function (){
    expect(
        replaceVowel("khandbari")).toBe("kh1ndb1r3")
})

it('LexiCAl', function (){
    expect(
        replaceVowel("LexiCAl")).toBe("l2x3c1l")
})

it('fuNctionS', function (){
    expect(
        replaceVowel("fuNctionS")).toBe("f5nct34ns")
})

it('EASY', function (){
    expect(
        replaceVowel("EASY")).toBe("21sy")
})

