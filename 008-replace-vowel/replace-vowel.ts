export function replaceVowel(input: string){
    const vowelValues = {
        "a": 1,
        "e": 2,
        "i": 3,
        "o": 4,
        "u": 5
    }

    let charArray = input.toLowerCase().split("")
    for(let i = 0; i < charArray.length; i++){
        if(vowelValues[charArray[i]] != undefined){
            charArray[i] = vowelValues[charArray[i]]
        }
    }

    return charArray.join("")
}