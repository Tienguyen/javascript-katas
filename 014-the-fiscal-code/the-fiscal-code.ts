const months = { 1: "A", 2: "B", 3: "C", 4: "D", 5: "E", 6: "H", 7:
        "L", 8: "M", 9: "P", 10: "R", 11: "S", 12: "T" }

const vowels = ["A", "E", "I", "O", "U"]

function isVowel(character){
    if(vowels.includes(character)){
        return true
    }
    return false
}

function handleSurename(surename){
    const upperSurename = surename.toUpperCase()
    let shortenedSurename = ""
    let surenameVowels = []
    for(let character of upperSurename){
        if(!isVowel(character)){
            shortenedSurename += character
        }else{
            surenameVowels.push(character)
        }
        if(shortenedSurename.length == 3){
            return shortenedSurename
        }
    }

    for(let vowel of surenameVowels){
        shortenedSurename += vowel
        if(surenameVowels.length == 3){
            return shortenedSurename
        }
    }

    return shortenedSurename  + "X"
}


function handleFirstname(firstname){
    const upperFirstname = firstname.toUpperCase()
    let shortenedFirstname = ""
    let firstnameVowels = []
    let firstnameConsonants = []
    for(let character of upperFirstname){
        if(!isVowel(character)){
            firstnameConsonants.push(character)
        }else{
            firstnameVowels.push(character)
        }
    }

    if(firstnameConsonants.length == 3){
        return firstnameConsonants.join("")
    }else if(firstnameConsonants.length > 3){
        return firstnameConsonants[0] + firstnameConsonants[2] + firstnameConsonants[3]
    }else{
        shortenedFirstname = firstnameConsonants.join("")

        for(let vowel of firstnameVowels){
            shortenedFirstname += vowel
            if(shortenedFirstname.length == 3){
                return shortenedFirstname
            }
        }
    }

    return shortenedFirstname + "X"
}

//date of birth: 28/05/1997
function handleYearAndGender(dateOfBirth, gender){
    let dateArray = dateOfBirth.split("/")
    let birthYear = dateArray[2].slice(-2)
    let monthLetter = months[dateArray[1]]


    let day = ""
    if(gender == "M"){
        if(dateArray[0].length == 1){
            day += "0"
        }
        day += dateArray[0]
    }else if(gender == "F"){
        day = parseInt(dateArray[0]) + 40
    }

    return birthYear + monthLetter + day

}


export function fiscalCode(person){
    let code = handleSurename(person.surname)
        + handleFirstname(person.name)
        + handleYearAndGender(person.dob, person.gender)

    return code;
}
