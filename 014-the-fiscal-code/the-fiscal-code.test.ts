import {fiscalCode} from "./the-fiscal-code";
import {calculateTwoPointers} from "../002-basketball/basketball";

it('test 1', function (){
    let matt = ({
        name: "Matt",
        surname: "Edabit",
        gender: "M",
        dob: "1/1/1900"
    })
    expect(
        fiscalCode(matt)).toBe("DBTMTT00A01")
})

it('test 2', function (){
    let mickey = ({
        name: "Mickey",
        surname: "Mouse",
        gender: "M",
        dob: "16/1/1928"
    })
    expect(
        fiscalCode(mickey)).toBe("MSOMKY28A16")
})

it('test 3', function (){
    let helen = ({
        name: "Helen",
        surname: "Yu",
        gender: "F",
        dob: "1/12/1950"
    })
    expect(
        fiscalCode(helen)).toBe("YUXHLN50T41")
})

