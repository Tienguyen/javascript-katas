export function bestFriend(sentence: string, first: string, second: string){
    if(!sentence.includes(first) || !sentence.includes(second) ){
        return false;
    }
    var firstSecond = new RegExp(first+second, "g");
    var filteredString = sentence.replace(firstSecond, "");

    if(filteredString.includes(first)){
        return false;
    }
    return true;
}