import { bestFriend } from './letters-best-friend';

it('("he headed to the store", "h", "e") should return true', function (){
    expect(
        bestFriend("he headed to the store", "h", "e")).toBe(true)
})

it('("i found an ounce with my hound", "o", "u") should return true', function (){
    expect(
        bestFriend("i found an ounce with my hound", "o", "u")).toBe(true)
})

it('("we found your dynamite", "d", "y") should return false', function (){
    expect(
        bestFriend("we found your dynamite", "d", "y")).toBe(false)
})

