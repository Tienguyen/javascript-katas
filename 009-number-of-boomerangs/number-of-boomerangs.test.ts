import {countBoomerangs} from "./number-of-boomerangs";

it('input [9, 5, 9, 5, 1, 1, 1], expected 2', function (){
    expect(
        countBoomerangs([9, 5, 9, 5, 1, 1, 1])).toBe(2)
})

it('input [5, 6, 6, 7, 6, 3, 9], expected 1\'', function (){
    expect(
        countBoomerangs([5, 6, 6, 7, 6, 3, 9]) ).toBe(1)
})


it('input [4, 4, 4, 9, 9, 9, 9], expected 0\'', function (){
    expect(
        countBoomerangs([4, 4, 4, 9, 9, 9, 9])).toBe(0)
})


it('input [1, 7, 1, 7, 1, 7, 1], expected 5\'', function (){
    expect(
        countBoomerangs([1, 7, 1, 7, 1, 7, 1])).toBe(5)
})