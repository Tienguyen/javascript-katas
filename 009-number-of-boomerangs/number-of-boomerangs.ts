export function countBoomerangs(input: number[]): number {
    let boomerangCounter:number = 0;
    for(let i = 0; i < input.length-2; i++){
        let threeLetters = input.filter((element,index) => index >= i && index <= i+2 )
        if(isBoomerang(threeLetters)){
            boomerangCounter++;
        }
    }
    return boomerangCounter;
}

function isBoomerang(input: number[]): boolean {
    if(input.length == 3 && input[0] == input[2] && input[1] != input[0]){
        return true
    }
    return false;
}