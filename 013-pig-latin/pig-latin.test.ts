import { pigLatinSentence } from "./pig-latin";

it("this is pig latin", function (){
    expect(
        pigLatinSentence("this is pig latin")).toBe("isthay isway igpay atinlay" )
})

it("wall street journal", function (){
    expect(
        pigLatinSentence("wall street journal")).toBe("allway eetstray ournaljay" )
})

it("raise the bridge", function (){
    expect(
        pigLatinSentence("raise the bridge")).toBe("aiseray ethay idgebray" )
})

it("all pigs oink", function (){
    expect(
        pigLatinSentence("all pigs oink")).toBe("allway igspay oinkway" )
})