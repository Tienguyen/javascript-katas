export function pigLatinSentence(input){
    const vowels = ["a", "e", "i", "o", "u"]
    let latinSentence = ""
    let originalWords = input.split(" ")
    for(let word of originalWords){
        if(vowels.includes(word[0])){
            latinSentence += word + "way"
        }else{
            for(let i = 0; i < word.length; i++){
                if(vowels.includes(word[i])){
                    latinSentence += word.substring(i, word.length) +
                        word.substring(0, i) + "ay"
                    break
                }
            }
        }
        latinSentence += " "
    }
    latinSentence = latinSentence.substring(0, latinSentence.length - 1)
    return latinSentence
}