export function calculateTwoPointers(twoPointers) {
    return twoPointers * 2;
}

export function calculateThreePointers(threePointers) {
    return threePointers * 3;
}

export function calculateScore(twoPoints, threePoints) {
    return calculateTwoPointers(twoPoints) + calculateThreePointers(threePoints);
}