import { calculateTwoPointers, calculateThreePointers, calculateScore } from "./basketball";

it('should times 2 by 5', function (){
    expect(
        calculateTwoPointers(5)).toBe(10)
})

it('should times 3 by 5', function (){
    expect(
        calculateThreePointers(5)).toBe(15)
})

it('should return 25', function (){
    expect(
        calculateScore(5, 5 )).toBe(25)
})