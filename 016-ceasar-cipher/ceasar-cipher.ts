function between(x, min, max) {
    return x > min && x < max;
}

function sipherLetter(letter, factor){
    let letterAscii = letter.charCodeAt(0)
    if(letterAscii < 65 || letterAscii > 122 || between(letterAscii, 90, 97)){
        return letter
    }

    let isUpper = true
    if(letter == letter.toLowerCase()){
        isUpper = false
    }

    let alteredAscii = letterAscii + factor
    if(!isUpper && alteredAscii > 122){
        alteredAscii = alteredAscii - 26
    }else if(isUpper && alteredAscii > 90){
        alteredAscii = alteredAscii - 26
    }

    return String.fromCharCode(alteredAscii)
}

export function ceasarSipher(input, factor){
    let returnString = ""
    for(let letter of input){
        returnString += sipherLetter(letter, factor)
    }
    return returnString
}