import {ceasarSipher} from "./ceasar-cipher";

it('should cipher with factor 5', function (){
    expect(
        ceasarSipher("Always-Look-on-the-Bright-Side-of-Life", 5))
        .toBe("Fqbfdx-Qttp-ts-ymj-Gwnlmy-Xnij-tk-Qnkj")
})

it('should cipher with factor 20', function (){
    expect(
        ceasarSipher("A friend in need is a friend indeed", 20))
        .toBe("U zlcyhx ch hyyx cm u zlcyhx chxyyx")
})

