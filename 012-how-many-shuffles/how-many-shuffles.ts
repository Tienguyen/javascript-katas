export function shuffleCount(cardCount){

    if(cardCount%2 != 0 || cardCount < 2 || cardCount > 52){
        throw 'Illegal argument!'
    }

    const cards = Array.from(Array(cardCount).keys())
    const half = Math.ceil(cards.length / 2);
    let firstHalf = cards.slice(0, half)
    let secondHalf = cards.slice(-half)
    let tempArray = []
    let count = 0;
    while(tempArray.toString()!==cards.toString()){
        tempArray = []
        for(let i = 0; i < firstHalf.length; i++){
            tempArray.push(firstHalf[i])
            tempArray.push(secondHalf[i])
        }
        firstHalf = tempArray.slice(0, half)
        secondHalf = tempArray.slice(-half)
        count ++
    }
    return count
}
