import { shuffleCount } from "./how-many-shuffles";

it('8 cards, should return 3', function (){
    expect(
        shuffleCount(8)).toBe(3)
})

it('14 cards, should return 12', function (){
    expect(
        shuffleCount(14)).toBe(12)
})

it('8 cards, should return 3', function (){
    expect(
        shuffleCount(52)).toBe(8)
})