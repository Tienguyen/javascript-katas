export function happyAlgorithm(input: number) :string{
    const [nLoops, isHappy]: [number, boolean] = calculateHappy(input)
    if(isHappy){
        return "HAPPY " + nLoops
    }
    return "SAD " + nLoops
}

function calculateHappy(input: number, nLoops: number = 0, copies: number[] = [input]): [number, boolean]{
    nLoops ++;
    let squaredSum = numbertoDigitSum(input)
    if(squaredSum == 1){
        return [nLoops, true];
    }

    if(copies.includes(squaredSum)){
        return [nLoops, false]
    }
    copies.push(squaredSum)
    return calculateHappy(squaredSum, nLoops, copies)
}

function numbertoDigitSum(input: number){
    const digitStrings: string[] = input.toString().split('');
    const digitNumbers: number[] = digitStrings.map(Number)
    let squaredSum: number = 0
    for(let digit of digitNumbers){
        squaredSum += Math.pow(digit, 2)
    }
    return squaredSum
}

function fasterCalculateHappy(input: number): [number ,boolean]{
    const sadNumbers = [16, 37, 58, 89, 145, 42, 20]
    let squaredSum = numbertoDigitSum(input)
    if(sadNumbers.includes(squaredSum)){
        return [8, false]
    }
    let count = 1;
    while(squaredSum != 1){
        if(sadNumbers.includes(squaredSum)){
            return [count + 8, false]
        }
        count ++
        squaredSum = numbertoDigitSum(squaredSum)
    }
    return [count, true]
}

export function testHappy(input: number){
    const [nLoops, isHappy]: [number, boolean] = fasterCalculateHappy(input)
    if(isHappy){
        return "HAPPY " + nLoops
    }
    return "SAD " + nLoops
}