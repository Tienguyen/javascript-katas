import {happyAlgorithm, testHappy} from "./the-happy-function";

it('ORIGINAL: Input 139, should return "HAPPY 5', function (){
    expect(
        happyAlgorithm(139)).toBe("HAPPY 5")
})


it('ORIGINAL: Input 67, should return "SAD 10', function (){
    expect(
        happyAlgorithm(67)).toBe("SAD 10")
})

it('ORIGINAL: Input 1, should return "HAPPY 1', function (){
    expect(
        happyAlgorithm(1)).toBe("HAPPY 1")
})

it('ORIGINAL: Input 89, should return "SAD 8', function (){
    expect(
        happyAlgorithm(89)).toBe("SAD 8")
})

it('NEW: Input 139, should return "HAPPY 5', function (){
    expect(
        testHappy(139)).toBe("HAPPY 5")
})


it('NEW: Input 67, should return "SAD 10', function (){
    expect(
        testHappy(67)).toBe("SAD 10")
})

it('NEW: Input 1, should return "HAPPY 1', function (){
    expect(
        testHappy(1)).toBe("HAPPY 1")
})

it('NEW: Input 89, should return "SAD 8', function (){
    expect(
        testHappy(89)).toBe("SAD 8")
})